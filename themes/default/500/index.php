<?php
/**
 * @created 20.10.12 - 09:39
 * @author stefanriedel
 */?>


<p>
    <?php echo __('Es ist ein Fehler aufgetreten, ein Administrator wurde informiert.') ?>
    <?php echo \Html::anchor(\Uri::create('/'), __('Zurück zum Dashboard')) ?>
</p>