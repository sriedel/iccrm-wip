<?php
/**
 * @created 19.10.12 - 12:53
 * @author stefanriedel
 */?>

<p>
    <?php echo __('Die Seite wurde leider nicht gefunden.') ?>
    <?php echo \Html::anchor(\Uri::create('/'), __('Zurück zum Dashboard')) ?>
</p>