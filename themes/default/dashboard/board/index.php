<?php
/**
 * @created 30.09.12 - 20:52
 * @author stefanriedel
 */
?>

<?php

if (is_array($dashboard_items) && count($dashboard_items) > 0) {

    $opened = false;

    foreach ($dashboard_items as $i => $item) {

        if (0 === $i % 3) {
            echo ($opened) ? '</div>' : '';
            echo '<div class="row" >';
            $opened = true;
        }

        echo '<div id="item-' . $i . '" class="span4">' . $item . '</div>';

        /**try
        {
        \Request::forge($item->route, false)->execute()->response()->body;
        }
        catch (\Exception $e)
        {
        \Debug::dump($e);
        }**/
    }

    echo "</div>";
}

?>

<script type="text/javascript">

    $('div.dashitem').each(function (a, b, c) {
        var $this = $(b);
        $.ajax({
            url: $this.attr('rel'),
            dataType: 'json',
            success: function(a, b, c){
                console.log(a, b, c);
            }
        });
    });

</script>