<?php

namespace Core;
/**
 * @created 28.09.12 - 09:57
 * @author stefanriedel
 */
class Controller_Base_Template_Blank extends \Core\Controller_Base_Template {
    public $template = 'templates/blank';
}