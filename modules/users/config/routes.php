<?php
/**
 * @created 12.10.12 - 11:35
 * @author stefanriedel
 */

return array(
    'users/password/confirmed_email/:hash' => 'users/password/confirmed_email'
);